Python NLTK installation info
-----------------------------
* Operating System Ubuntu Trusty x64
* Python 2.7.6
* All NLTK corpus collections are downloaded for the 'root' user. So switch to 'root' user for using NLTK